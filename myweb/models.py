from django.db import models

class Message(models.Model):
    activity = models.CharField(max_length = 27)
    location = models.CharField(max_length=25)
    start_date = models.DateField(default='1990-01-01')
    start_time = models.TimeField(default='12:00')
    end_date = models.DateField(default='1990-01-01')
    end_time = models.TimeField(default='12:00')
    notes = models.TextField()
    created_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.activity
# Create your models here.
